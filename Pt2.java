package actividad.pkg3.arreglos.multidimensionales;

public class Programa_2 {
    static String[][] aCodigo;
  
    public static void Arreglo(){
        aCodigo = new String[26][2];
 
        
        aCodigo[0][0] = "A";
        aCodigo[1][0] = "B";
        aCodigo[2][0] = "C";
        aCodigo[3][0] = "D";
        aCodigo[4][0] = "E";
        aCodigo[5][0] = "F";
        aCodigo[6][0] = "G";
        aCodigo[7][0] = "H";
        aCodigo[8][0] = "I";
        aCodigo[9][0] = "J";
        aCodigo[10][0] = "K";
        aCodigo[11][0] = "L";
        aCodigo[12][0] = "M";
        aCodigo[13][0] = "N";
        aCodigo[14][0] = "O";
        aCodigo[15][0] = "P";
        aCodigo[16][0] = "Q";
        aCodigo[17][0] = "R";
        aCodigo[18][0] = "S";
        aCodigo[19][0] = "T";
        aCodigo[20][0] = "U";
        aCodigo[21][0] = "V";
        aCodigo[22][0] = "W";
        aCodigo[23][0] = "X";
        aCodigo[24][0] = "Y";
        aCodigo[25][0] = "Z";
       
        aCodigo[0][1] = "65";
        aCodigo[1][1] = "66";
        aCodigo[2][1] = "67";
        aCodigo[3][1] = "68";
        aCodigo[4][1] = "69";
        aCodigo[5][1] = "70";
        aCodigo[6][1] = "71";
        aCodigo[7][1] = "72";
        aCodigo[8][1] = "73";
        aCodigo[9][1] = "74";
        aCodigo[10][1] = "75";
        aCodigo[11][1] = "76";
        aCodigo[12][1] = "77";
        aCodigo[13][1] = "78";
        aCodigo[14][1] = "79";
        aCodigo[15][1] = "80";
        aCodigo[16][1] = "81";
        aCodigo[17][1] = "82";
        aCodigo[18][1] = "83";
        aCodigo[19][1] = "84";
        aCodigo[20][1] = "85";
        aCodigo[21][1] = "86";
        aCodigo[22][1] = "87";
        aCodigo[23][1] = "88";
        aCodigo[24][1] = "89";
        aCodigo[25][1] = "90";       
    }
    
    public static void printMatriz(){
         System.out.println("=======================");
        for (int i=0;i <26; i++){
            System.out.println(aCodigo[i][0]+ " = "+ aCodigo[i][1]);
        }
        
    }    
    public static void FraseAscii(String sFrase){
        
        char[] aChar = sFrase.toCharArray();
        int asciiValue;
       
        for (char c: aChar){
            asciiValue = (int) c;
            System.out.println(c+ " = "+ aCodigo[asciiValue-65][1]);
        }
       
    }    
    
    public static void main(String[] args) throws Exception {
        Arreglo();
        
        String sCadena = "CRISTIAN";
        FraseAscii(sCadena);
        FraseAscii("ALEXANDER");
       FraseAscii("WILLIAM");
        printMatriz();

}
}
